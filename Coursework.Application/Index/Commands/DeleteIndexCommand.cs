﻿using Coursework.Persistence;
using Coursework.Persistence.Mongo;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Application.Index.Commands
{
    public class DeleteIndexCommand: IRequest<Unit>
    {
        class Handler : IRequestHandler<DeleteIndexCommand, Unit>
        {
            private readonly IIndexRepository indexRepository;
            public Handler(IIndexRepository indexRepository)
            {
                this.indexRepository = indexRepository;
            }

            public async Task<Unit> Handle(DeleteIndexCommand request, CancellationToken cancellationToken)
            {
                await indexRepository.DeleteIndexAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
