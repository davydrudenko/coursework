﻿using Coursework.Application.Options;
using Coursework.Application.Parsers;
using Coursework.Domain.Entities;
using Coursework.Persistence.Cache;
using Coursework.Persistence.Mongo;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Application.Index.Commands
{
    public class BuildIndexCommand : IRequest<InverseIndex>
    {
        public int? ThreadCount { get; set; }
        public int? FilesCount { get; set; }
        public class Handler : IRequestHandler<BuildIndexCommand, InverseIndex>
        {
            private readonly IndexOptions options;
            private readonly ITextParser fileParser;
            private readonly ICache cache;
            private readonly IIndexRepository indexRepository;
            private static object obj = new object();
            public Handler(IOptions<IndexOptions> options, ICache cache, ITextParser fileParser, IIndexRepository indexRepository)
            {
                this.options = options.Value;
                this.cache = cache;
                this.fileParser = fileParser;
                this.indexRepository = indexRepository;
            }
            public Handler(IndexOptions options, ICache cache, ITextParser fileParser, IIndexRepository indexRepository)
            {
                this.options = options;
                this.cache = cache;
                this.fileParser = fileParser;
                this.indexRepository = indexRepository;
            }

            public async Task<InverseIndex> Handle(BuildIndexCommand request, CancellationToken cancellationToken)
            {
                var totalCount = request.FilesCount.HasValue ? request.FilesCount.Value :  await cache.GetFileCountAsync();
                var threadCount = request.ThreadCount.HasValue ? request.ThreadCount.Value : options.ThreadCount;
                var pageSize = totalCount / threadCount;
                var pageCount = totalCount / pageSize;
                var tasks = new List<Task>();
                var index = new Dictionary<string, List<string>>();
                for (int i = 0; i < pageCount; i++)
                {
                    var temp = i;
                    tasks.Add(Task.Run(async () =>
                    {
                        var page = await cache.GetPageAsync(temp + 1, pageSize);
                        var tokenDict = fileParser.Parse(page);
                        foreach (var kvpair in tokenDict)
                        {
                            lock (obj)
                            {
                                var existingToken = index.GetValueOrDefault(kvpair.Key);
                                if (existingToken == null)
                                {
                                    index[kvpair.Key] = kvpair.Value;
                                }
                                else
                                    index[kvpair.Key].AddRange(kvpair.Value);
                            }
                        }
                    }));
                }
                await Task.WhenAll(tasks);
                var invIndex = new InverseIndex(index);
                if (!options.TestRun)
                    await indexRepository.SetIndexAsync(invIndex, cancellationToken);
                return invIndex;
            }
        }
    }
}
