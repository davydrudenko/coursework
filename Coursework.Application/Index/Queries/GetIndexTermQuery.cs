﻿using Coursework.Application.Models;
using Coursework.Domain.Entities;
using Coursework.Persistence.Mongo;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Application.Index.Queries
{
    public class GetIndexTermQuery : IRequest<List<string>>
    {
        public string Key { get; set; }
        internal class Handler : IRequestHandler<GetIndexTermQuery, List<string>>
        {
            private readonly IIndexRepository indexRepository;

            public Handler(IIndexRepository indexRepository)
            {
                this.indexRepository = indexRepository;
            }

            public async Task<List<string>> Handle(GetIndexTermQuery request, CancellationToken cancellationToken)
            {
                var index = await indexRepository.GetIndexAsync();
                return index.GetEntries(new TextToken { Token = request.Key });
            }
        }
    }
}
