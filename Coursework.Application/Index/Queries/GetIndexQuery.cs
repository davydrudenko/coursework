﻿using Coursework.Domain.Entities;
using Coursework.Persistence.Mongo;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Application.Index.Queries
{
    public class GetIndexQuery: IRequest<InverseIndex>
    {
        public class Handler : IRequestHandler<GetIndexQuery, InverseIndex>
        {
            private readonly IIndexRepository indexRepository;

            public Handler(IIndexRepository indexRepository)
            {
                this.indexRepository = indexRepository;
            }

            public async Task<InverseIndex> Handle(GetIndexQuery request, CancellationToken cancellationToken)
            {
                return await indexRepository.GetIndexAsync();
            }
        }
    }
}
