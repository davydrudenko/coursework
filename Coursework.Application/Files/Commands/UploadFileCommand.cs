﻿using Coursework.Application.Models;
using Coursework.Domain.Entities;
using Coursework.Persistence;
using MediatR;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Application.Files.Commands
{
    public class UploadFileCommand: IRequest<Unit>
    {
        public FileUploadModel Upload { get; set; }
        internal class Handler : IRequestHandler<UploadFileCommand, Unit>
        {
            public async Task<Unit> Handle(UploadFileCommand request, CancellationToken cancellationToken)
            {
                var filePath = Path.Combine(request.Upload.BasePath, "FileCache", request.Upload.FileName);
                await File.WriteAllLinesAsync(filePath, new string[] { request.Upload.Content });
                return Unit.Value;
            }
        }
    }
}
