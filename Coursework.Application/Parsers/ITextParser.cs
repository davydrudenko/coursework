﻿using Coursework.Domain.Entities;
using System.Collections.Generic;

namespace Coursework.Application.Parsers
{
    public interface ITextParser
    {
        Dictionary<string, List<string>> Parse(IEnumerable<IndexFile> files);
    }
}
