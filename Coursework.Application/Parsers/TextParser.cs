﻿using Coursework.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Coursework.Application.Parsers
{
    public class TextParser : ITextParser
    {
        private readonly string pattern = "[\\w]+";
        public Dictionary<string, List<string>> Parse(IEnumerable<IndexFile> files)
        {
            var dict = new Dictionary<string, List<string>>();
            foreach (var file in files)
            {
                var tokenized = tokenize(file.Content);
                foreach(var token in tokenized)
                {
                    if (dict.ContainsKey(token))
                        dict[token].Add(file.FileName);
                    else
                        dict[token] = new List<string> { file.FileName };
                }
            }
            return dict;
        }

        private List<string> tokenize(string text)
        {
            Dictionary<string, object> tokens = new Dictionary<string, object>();
            var regex = new Regex(pattern);
            foreach (Match match in regex.Matches(text))
            {
                if (!tokens.ContainsKey(match.Value))
                    tokens[match.Value] = new object();

            }
            return tokens.Keys.ToList();
        }
    }
}
