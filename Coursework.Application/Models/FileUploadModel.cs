﻿namespace Coursework.Application.Models
{
    public class FileUploadModel
    {
        public string FileName { get; set; }
        public string Content { get; set; }
        public string BasePath { get; set; }
    }
}
