﻿namespace Coursework.Application.Options
{
    public class IndexOptions
    {
        public int ThreadCount { get; set; }
        public bool TestRun { get; set; }
    }
}
