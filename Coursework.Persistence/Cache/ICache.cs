﻿using Coursework.Domain.Entities;
using Coursework.Persistence.Cache.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coursework.Persistence.Cache
{
    public interface ICache
    {
        Task<IndexFile> GetIndexFileAsync(string fileName);
        Task<List<IndexFile>> GetPageAsync(int pageNumber, int pageSize);
        Task<int> GetFileCountAsync();

    }
}
