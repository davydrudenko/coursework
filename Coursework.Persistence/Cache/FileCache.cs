﻿using Coursework.Domain.Entities;
using Coursework.Persistence.Options;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DirectoryInfo = Coursework.Persistence.Cache.Models.DirectoryInfo;
namespace Coursework.Persistence.Cache
{
    public class FileCache : ICache
    {
        private readonly CacheOptions options;
        private static List<string> files = new List<string>();
        private static int fileCount = 0;
        public FileCache(IOptions<CacheOptions> options)
        {
            this.options = options.Value;
            var info = TraverseAsync(options.Value.CacheDir).GetAwaiter().GetResult();
            files = info.FilePaths;
        }

        public FileCache(CacheOptions options)
        {
            this.options = options;
            var info = TraverseAsync(options.CacheDir).GetAwaiter().GetResult();
            files = info.FilePaths;
        }

        private async Task<DirectoryInfo> TraverseAsync(string path)
        {
            if (!Directory.Exists(path))
                throw new ArgumentException($"directory {path} does not exist");

            var dInfo = new DirectoryInfo();
            var tasks = new List<Task<DirectoryInfo>>();

            foreach (var file in Directory.GetFiles(path))
            {
                dInfo.FilePaths.Add(Path.GetFullPath(file));
                ++dInfo.FileCount;

            }

            foreach (var directory in Directory.GetDirectories(path))
            {
                var temp = directory;
                var traverse = Task.Run(() =>
                {
                    return TraverseAsync(temp);
                });
                tasks.Add(traverse);
            }

            await Task.WhenAll(tasks);

            return tasks.Aggregate(dInfo, (prev, curr) =>
             {
                 var result = curr.Result;
                 prev.FileCount += result.FileCount;
                 prev.FilePaths.AddRange(result.FilePaths);
                 return prev;
             });
        }

        public Task<IndexFile> GetIndexFileAsync(string fileName)
        {
            throw new NotImplementedException();
        }

        public async Task<List<IndexFile>> GetPageAsync(int pageNumber, int pageSize)
        {
            var paginated = files.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return paginated.AsParallel().Select(x =>
            {
                return new IndexFile
                {
                    FileName = x,
                    Content = ReadFileContents(x)
                };
            }).ToList();
        }

        private string ReadFileContents(string path)
        {
            return File.ReadAllText(path);
        }

        public async Task<int> GetFileCountAsync()
        {
            if(fileCount == 0)
            {
                var info = await TraverseAsync(options.CacheDir);
                files = info.FilePaths;
                fileCount = info.FileCount;
            }

            return fileCount;
        }
    }
}
