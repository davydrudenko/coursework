﻿using Coursework.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coursework.Persistence.Configurations
{
    class IndexFileConfiguration : IEntityTypeConfiguration<IndexFile>
    {
        public void Configure(EntityTypeBuilder<IndexFile> builder)
        {
            builder.Property(x => x.FileName).IsRequired();
            builder.Property(x => x.Content).IsRequired();
            builder.HasIndex(x => x.FileName);
        }
    }
}
