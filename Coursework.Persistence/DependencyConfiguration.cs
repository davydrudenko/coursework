﻿using Coursework.Persistence.Mongo.Serializers;
using MongoDB.Bson.Serialization;

namespace Coursework.Persistence
{
    public static class DependencyConfiguration
    {
        public static void AddMongo()
        {
            BsonSerializer.RegisterSerializer(new FileInfoSerializer());
        }
    }
}
