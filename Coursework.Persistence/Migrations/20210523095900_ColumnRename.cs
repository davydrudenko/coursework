﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Coursework.Persistence.Migrations
{
    public partial class ColumnRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Contents",
                table: "IndexFiles",
                newName: "Content");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Content",
                table: "IndexFiles",
                newName: "Contents");
        }
    }
}
