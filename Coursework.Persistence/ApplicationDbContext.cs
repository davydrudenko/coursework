﻿using Coursework.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Coursework.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<IndexFile> IndexFiles { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}
