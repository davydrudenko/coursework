﻿using Coursework.Domain.Entities;
using MongoDB.Bson.Serialization;
using System;

namespace Coursework.Persistence.Mongo.Serializers
{
    class FileInfoSerializer : IBsonSerializer<FileInfo>
    {
        public Type ValueType => typeof(FileInfo);

        public FileInfo Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var s = context.Reader.ReadString();
            return new FileInfo(s);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, FileInfo value)
        {
            context.Writer.WriteString(value.ToString());
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            Serialize(context, args, (FileInfo)value);
        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            return this.Deserialize(context, args);
        }
    }
}
