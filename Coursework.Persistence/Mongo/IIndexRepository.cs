﻿using Coursework.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Persistence.Mongo
{
    public interface IIndexRepository
    {
        public Task<InverseIndex> GetIndexAsync();
        public Task SetIndexAsync(InverseIndex index, CancellationToken cancellationToken);
        public Task DeleteIndexAsync(CancellationToken cancellationToken);
    }
}
