﻿using Coursework.Domain.Entities;
using Coursework.Persistence.Mongo.Serializers;
using Coursework.Persistence.Options;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace Coursework.Persistence.Mongo
{
    public class IndexRepository : IIndexRepository
    {
        private readonly IMongoCollection<InverseIndex> indexes;
        private MongoDbOptions options;
        public IndexRepository(IOptions<MongoDbOptions> mongoOptions)
        {
            options = mongoOptions.Value;
            var client = new MongoClient(options.ConnectionString);
            var database = client.GetDatabase(options.Database);
            indexes = database.GetCollection<InverseIndex>(options.Collection);
            var indexKeysDefinition = Builders<InverseIndex>.IndexKeys.Ascending(x => x.BuildDate);
            indexes.Indexes.CreateOneAsync(new CreateIndexModel<InverseIndex>(indexKeysDefinition)).GetAwaiter().GetResult();
        }
        public IndexRepository()
        {

        }

        public async Task DeleteIndexAsync(CancellationToken cancellationToken)
        {
           await indexes.DeleteManyAsync(x => true);
        }

        public async Task<InverseIndex> GetIndexAsync()
        {
            return await indexes.Find(x => true).SortByDescending(x => x.BuildDate).Limit(1).FirstOrDefaultAsync();
        }

        public async Task SetIndexAsync(InverseIndex index, CancellationToken cancellationToken)
        {
            await indexes.InsertOneAsync(index, new InsertOneOptions { BypassDocumentValidation = false }, cancellationToken);
        }
    }
}
