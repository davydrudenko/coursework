﻿using System.Collections.Generic;

namespace Coursework.Persistence.Cache.Models
{
    public class DirectoryInfo
    {
        public int FileCount { get; set; }
        public List<string> FilePaths { get; set; }

        public DirectoryInfo()
        {
            FileCount = 0;
            FilePaths = new List<string>();
        }
    }
}
