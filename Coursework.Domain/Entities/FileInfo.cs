﻿using System.IO;

namespace Coursework.Domain.Entities
{
    public class FileInfo
    {
        public string Directory { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public FileInfo(string filePath)
        {

            Name = Path.GetFileName(filePath);
            Directory = Path.GetDirectoryName(filePath);
            Extension = Path.GetExtension(filePath);
        }
        public override string ToString()
        {
            return Path.Combine(Directory, $"{Name.Split('.')[0]}");
        }
    }
}
