﻿using System;

namespace Coursework.Domain.Entities
{
    public class IndexFile
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }
    }
}
