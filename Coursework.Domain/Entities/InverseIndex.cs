﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Coursework.Domain.Entities
{
    public class InverseIndex
    {
        [JsonIgnore]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTime BuildDate { get; set; }
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfDocuments)]
        private Dictionary<string, List<string>> index { get; set; }
        public InverseIndex(Dictionary<string, List<string>> index)
        {
            this.index = index;//index.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToDictionary(x => new FileInfo(x.Key), x => x.Value));
            BuildDate = DateTime.UtcNow;
        }
        public List<string> GetEntries(TextToken key)
        {
            var exists = index.TryGetValue(key.Token, out var value);
            return value;
        }
    }
}
