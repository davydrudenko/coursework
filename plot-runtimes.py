import os
import csv
import glob 
import matplotlib.pyplot as plt
run_results_folder = os.getcwd() + "/Tests/RunResults"
run_results_csv = glob.glob(f"{run_results_folder}/*-files.csv")
for runtime_measurement in run_results_csv:
    with open(runtime_measurement) as temp:
        reader = csv.reader(temp)
        data = [(int(row[0]), int(row[1])) for row in reader]
        plt.plot([x[0] for x in data], [y[1] for y in data], label = f"{runtime_measurement.split('/')[-1]}")
plt.xlabel("threads count")
plt.ylabel("runtime")
plt.legend()
plt.show()