﻿namespace Tests
{
    public class BenchmarkRunResult
    {
        public int NThreads { get; set; }
        public int NFiles { get; set; }
        public long Millis { get; set; }
    }
}
