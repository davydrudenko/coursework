﻿using NUnit.Framework;
using FakeItEasy;
using Coursework.Application.Index.Commands;
using Coursework.Application.Options;
using Coursework.Persistence.Cache;
using Coursework.Persistence.Options;
using System.IO;
using Coursework.Persistence.Mongo;
using Coursework.Application.Parsers;
using System.Threading;
using System.Threading.Tasks;
using Coursework.Domain.Entities;

namespace Tests
{
    [TestFixture]
    class IndexTests
    {
        BuildIndexCommand.Handler handler;
        
        [Test]
        public async Task BuildTestIndex_NoParams_ShouldReturnValidIndex()
        {
            Setup(1);
            var request = new BuildIndexCommand();
            var index = await handler.Handle(request, CancellationToken.None);
            
            var aEntries = index.GetEntries(new TextToken { Token = "a" });
            var bEntries = index.GetEntries(new TextToken { Token = "b" });
            
            Assert.AreEqual(3, aEntries.Count);
            Assert.AreEqual(2, bEntries.Count);
        }

        [Test]
        public async Task BuildTestIndex_MultipleThreads_ShouldReturnValidIndex()
        {
            Setup(2);
            var request = new BuildIndexCommand();
            var index = await handler.Handle(request, CancellationToken.None);

            var aEntries = index.GetEntries(new TextToken { Token = "a" });
            var bEntries = index.GetEntries(new TextToken { Token = "b" });

            Assert.AreEqual(3, aEntries.Count);
            Assert.AreEqual(2, bEntries.Count);
        }

        private void Setup(int numThreads)
        {

            var indexOptions = new IndexOptions
            {
                ThreadCount = numThreads,
                TestRun = true
            };
            var cacheOptions = new CacheOptions
            {
                CacheDir = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + "\\TestDirectory")
            };

            var cache = new FileCache(cacheOptions);
            var repository = A.Fake<IIndexRepository>();
            var parser = new TextParser();
            handler = new BuildIndexCommand.Handler(indexOptions, cache, parser, repository);
        }
    }
}
