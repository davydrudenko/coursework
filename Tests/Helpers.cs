﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Tests
{
    internal static class Helpers
    {

        private static readonly string[] csvHeaders = { "NThreads", "Runtime" };

        public static string GetPath(string relativePath)
        {
            return Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + (relativePath.StartsWith("\\") ? relativePath : $"\\{relativePath}"));
        }

        public static void SaveRun(BenchmarkRunResult result, string baseDir)
        {
            using var mem = new FileStream(Path.Combine(baseDir, $"{result.NFiles}-files.csv"), FileMode.Append);
            using var writer = new StreamWriter(mem);
            using var csvWriter = new CsvWriter(writer, CultureInfo.CurrentCulture);

            csvWriter.WriteField(result.NThreads);
            csvWriter.WriteField(result.Millis);
            csvWriter.NextRecord();

            writer.Flush();
        }

    }
}
