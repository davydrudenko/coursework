﻿using Coursework.Application.Index.Commands;
using Coursework.Application.Options;
using Coursework.Application.Parsers;
using Coursework.Persistence.Cache;
using Coursework.Persistence.Mongo;
using Coursework.Persistence.Options;
using FakeItEasy;
using NUnit.Framework;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
namespace Tests
{
    [TestFixture]
    class TimingTests
    {
        [Test]
        public async Task TimeTests()
        {
            var testTasks = new List<Task>();
            var csvDirectory = Helpers.GetPath("RunResults");
            object obj = new object();
            for (int i = 1_000; i <= 15_000; i += 2_000)
            {
                int fileCount = i;
                for (int j = 1; j <= 10; j++)
                {
                    var request = new BuildIndexCommand
                    {
                        FilesCount = fileCount,
                        ThreadCount = j,
                    };
                    var handler = SetupHandler();
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    handler.Handle(request, CancellationToken.None).GetAwaiter().GetResult();
                    sw.Stop();
                    Helpers.SaveRun(new BenchmarkRunResult { NThreads = j, NFiles = fileCount, Millis = (long)sw.ElapsedMilliseconds }, csvDirectory);
                }
            }
        }

        private BuildIndexCommand.Handler SetupHandler(int numThreads = 1)
        {
            var indexOptions = new IndexOptions
            {
                ThreadCount = numThreads,
                TestRun = true
            };
            var cacheOptions = new CacheOptions
            {
                CacheDir = Helpers.GetPath("BenchmarkDirectory")
            };

            var cache = new FileCache(cacheOptions);
            var repository = A.Fake<IIndexRepository>();
            var parser = new TextParser();
            return new BuildIndexCommand.Handler(indexOptions, cache, parser, repository);
        }

    }
}
