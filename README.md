### Parallel computing coursework

#### 1. Introduction

----

This is a rather simplistic and a bit unfinished project because there are quite a few changes that I would like to make in order to not be ashamed of showing this code to my co-workers(hope they will never find it :-).

This repository contains source code for my coursework and is developed based on the RESTful Api principles and ASP .NET Core 3.1 architectural guidelines. 

This project heavily relies on Mediator pattern, and is inspired by the [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). In order to run it, you will need Docker Desktop in order to run MongoDb, since this application uses it to persist inverse index once it had been built. Even though you may find some clues that at some point a SQL Server was used to store the uploaded files, now it is gone because it would be terribly inefficient.

#### 2. How to run this project

-----

- Step 1: Clone the repository

  In order to clone the repo to your local machine, run `git clone https://gitlab.com/davydrudenko/coursework.git`

- Step 2: Opening Solution in Visual Studio

  In order to navigate around the project easily, run tests and RESTful Api you should install Visual Studio. In order to load the project in Visual Studio, open Coursework.sln file in the root directory. If everything goes well, Visual Studio will install all of the necessary dependencies for you and load the project in seconds. 

- Step 3: Running MongoDb in Docker Desktop

  To run the backend api services, you will need a MongoDb instance running at `localhost:27017`, although you can change that to whichever connection string that works for you by modifying `Mongo` section in `Coursework/appsettings.Development.json`. In order to run it locally via Docker Desktop, there is a `docker-compose.yml` file under the `Coursework.Persistence` folder. To start MongoDb locally, `cd` to `Coursework.Persistence` and run `docker-compose up`. These commands should start a MongoDb instance in Docker container and create a user with a rather limited access(although, sufficient for the task at hand).

- Step 4: Run tests

  A few unit tests have been developed in order to verify that the main parts of the app work as intented both in single threaded and multi threaded environments.  These tests can be found in the `Tests` project, along with benchmarking results and graphical representation on the app's performance. To run the tests, click `Test > Run All Tests` button on the top bar. 

  This may take some time to run, since benchmarking had been implemented as a separate unit-test for convenience. Also, please make sure that `RunResults` directory is empty before running the benchmarks to avoid any confusing results.

- Interacting with the Api

  In order to use the Api from your browser without having to develop a separate frontend application, a Swagger documentation framework is used to expose existing endpoints to anyone curious enough right in their browser. To start using the Swagger's GUI, go to `https://localhost:5001/swagger/index.html` and try using the Api.

#### 3. My thoughts on what could have been done better and benchmarking results

----

Even though this section is mostly a reminder to my future self, I think it is worth leaving these notes here just in case I make the same mistakes again. Here are benchmarking results all plotted on the same graph:

![](Tests/RunResults/plots.png)

*Mistakes were made...*

- During my work I noticed that the main bottleneck occurs when we try to load a bunch(possibly, thousands) of files to memory. Even thought it seems like a no-brainer to parallelize that process, I don't like that idea, because it might lead to thread starvation and other issues that occur when you have to many threads running on your machine. Actually, we can clearly notice this issue on the benchmark plots, because when the number of threads increases we make more and more requests to the file system. I think the best solution to this problem would be using some sort of a caching server that would store pre-cached files, like `memcached`or `Redis`. 
- Also, I shouldn't have picked MongoDb as my persistence provider, since I use file names as keys and Mongo driver in .NET does not allow a key with a dot in the name to be saved to the database unless you create your own serializer, which is quite time consuming. On the other hand, we're dealing with a domain entity, whose structure, sizes and properties are not clearly defined, so I think it was a good decision to use NoSql Db.
- Another thing that I would like to mention is that I should not have spent so much time developing all of the unnecessary infrastructure, like SQL Server and event Redis to just throw them away in a couple of hours.  
