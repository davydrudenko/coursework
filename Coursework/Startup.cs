using Coursework.Application.Index.Commands;
using Coursework.Persistence.Options;
using Coursework.Application.Parsers;
using Coursework.Persistence;
using Coursework.Persistence.Cache;
using Coursework.Persistence.Mongo;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.IO;
using System.Reflection;
using Coursework.Application.Options;

namespace Coursework
{
    public class Startup
    {
        private readonly IWebHostEnvironment Environment;
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //services.AddDbContext<ApplicationDbContext>(options =>
                    //options.UseSqlServer(Configuration.GetConnectionString("ApplicationDb")));
            services.AddSwaggerGen(opt => opt.SwaggerDoc("coursework", new OpenApiInfo { Title = "Coursework API", Version = "V1" }));
            services.Configure<MongoDbOptions>(Configuration.GetSection("Mongo"));
            services.Configure<CacheOptions>(Configuration.GetSection("CacheOptions"))
                .Configure<CacheOptions>(x => x.CacheDir = Path.Combine(Environment.ContentRootPath, x.CacheDir));
            services.Configure<IndexOptions>(Configuration.GetSection("IndexOptions"));
            services.AddMediatR(typeof(BuildIndexCommand).GetTypeInfo().Assembly);
            services.AddScoped<IIndexRepository, IndexRepository>();
            services.AddScoped<ICache, FileCache>();
            services.AddScoped<ITextParser, TextParser>();
            DependencyConfiguration.AddMongo();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            app.UseSwaggerUI(opt => opt.SwaggerEndpoint("/swagger/coursework/swagger.json", "Coursework API"));
        }
    }
}
