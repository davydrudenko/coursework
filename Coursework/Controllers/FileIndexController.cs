﻿using Coursework.Application.Files.Commands;
using Coursework.Application.Index.Commands;
using Coursework.Application.Index.Queries;
using Coursework.Application.Models;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Coursework.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FileIndexController : ControllerBase
    {
        private readonly IMediator mediator;
        private readonly IWebHostEnvironment environment;
        public FileIndexController(IMediator mediator, IWebHostEnvironment environment)
        {
            this.mediator = mediator;
            this.environment = environment;
        }

        [HttpPost()]
        public async Task<IActionResult> BuildIndex(int? threads, int? files)
        {
            return Ok(await mediator.Send(new BuildIndexCommand()
            {
                ThreadCount = threads,
                FilesCount = files
            }));
        }

        [HttpGet("{key}")]
        public async Task<IActionResult> GetTerm(string key)
        {
            return Ok(await mediator.Send(new GetIndexTermQuery { Key = key }));
        }

        [HttpPut("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            var contents = string.Empty;
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                contents = reader.ReadToEnd();
            }
            await mediator.Send(new UploadFileCommand
            {
                Upload = new FileUploadModel
                {
                    FileName = file.FileName,
                    Content = contents,
                    BasePath = environment.ContentRootPath
                }
            });
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteIndex()
        {
            return Ok(await mediator.Send(new DeleteIndexCommand()));
        }
    }
}
